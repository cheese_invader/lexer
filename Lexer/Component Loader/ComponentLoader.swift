//
//  ComponentLoader.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ComponentLoader {
    func loadComponents() -> [Component]? {
        var components = [Component]()
        
        guard let filePaths = listFilesFromComponentFolder() else {
            print("ComponentLoader - loadComponents - can't find components directory")
            return nil
        }
        for filePath in filePaths {
            guard let component = loadComponent(filePath) else {
                print("ComponentLoader - loadComponents - can't read file \(filePath)")
                continue
            }
            components.append(component)
        }
        
        return components
    }
    
    private func loadComponent(_ fileURL: URL) -> Component? {
        do {
            let data = try Data(contentsOf: fileURL)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(ComponentData.self, from: data)
            return jsonData.component
        } catch {
            print("error:\(error.localizedDescription)")
            return nil
        }
    }
    
    private func listFilesFromComponentFolder() -> [URL]? {
        do {
            return try FileManager.default.contentsOfDirectory(at: Config.shared.componentsDir, includingPropertiesForKeys: nil, options: [])
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
