//
//  Config.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class Config {
    static let shared = Config()
    
    private init() {
        guard let dir = FileManager.default.urls(for: .desktopDirectory, in: .userDomainMask).first else {
            print("Config - init - Rule dir is incorrect")
            fatalError()
        }
    
        self.rulesDir = dir.appendingPathComponent("Lexer/Rules")
        self.componentsDir = self.rulesDir.appendingPathComponent("Components")
    }
    
    let rulesDir: URL
    let componentsDir: URL
    
    let anyChar: Character = "\u{0011}"
    let emptyChar: Character = "\u{0017}"
}
