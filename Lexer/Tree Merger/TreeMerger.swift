//
//  TreeMerger.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class TreeMerger {
    private var id = 1
    private var keyId = 1
    private var transKeys: [Character: Int] = ["\u{0012}": 0]
    private var idByKey: [Int: Character] = [0: "\u{0012}"]
    private var transTable = [[Set<Int>]]()
    private var transFinish = [Int: Int]()
    private var visited = Set<Int>()
    
    
    init(trees: [Leaf]) {
        for tree in trees {
            setIdsAndKeys(tree)
        }
        
        transTable = Array(repeating: Array(repeating: Set<Int>(), count: keyId), count: id)
        
        for tree in trees {
            transTable[0][0].insert(tree.id)
            visited = []
            generateTableForTree(tree)
        }
    }
    
    var table: [[Set<Int>]] {
        return transTable
    }
    
    var keys: [Character: Int] {
        return transKeys
    }
    
    var idsByKey: [Int: Character] {
        return idByKey
    }
    
    var finish: [Int: Int] {
        return transFinish
    }
    
    
    private func setIdsAndKeys(_ leaf: Leaf) {
        if leaf.id != -1 {
            return
        }
        if leaf.finalId != 0 {
            transFinish[id] = leaf.finalId
        }
        leaf.id = id
        id += 1
        
        for trans in leaf.trans {
            if transKeys[trans.key] == nil {
                idByKey[keyId] = trans.key
                transKeys[trans.key] = keyId
                keyId += 1
            }
            setIdsAndKeys(trans.value)
        }
    }
    
    private func generateTableForTree(_ tree: Leaf) {
        if visited.contains(tree.id) {
            return
        }
        visited.insert(tree.id)
        
        for trans in tree.trans {
            transTable[tree.id][transKeys[trans.key]!].insert(trans.value.id)
            generateTableForTree(trans.value)
        }
    }
    
}
