//
//  main.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

let loader = ComponentLoader()
guard var components = loader.loadComponents(), !components.isEmpty else {
    print("There is nothing to compose")
    fatalError()
}

var trees = [Leaf]()

for cmp in components {
    trees.append(Tree(cmp).getRoot())
}

let merger = TreeMerger(trees: trees)
let determinator = TreeDeterminator(table: merger.table, keys: merger.keys, finals: merger.finish)


let finals = merger.finish
let keys = merger.idsByKey
let table = determinator.transitTable
var st = determinator.indexByState

var leafs = [FinalLeaf]()

for _ in 0...table.count {
    leafs.append(FinalLeaf())
}

for final in finals {
    for state in st {
        if state.key.contains(final.key) {
            if leafs[state.value - 1].finalId != 0 {
                print("Main Final state error")
                fatalError()
            }
            leafs[state.value - 1].finalId = final.value
        }
    }
}


for (stateNumber, row) in table.enumerated() {
    for (keyNumber, col) in row.enumerated() where !col.isEmpty {
        if keys[keyNumber] == Config.shared.anyChar {
            leafs[stateNumber].any = leafs[st[col]! - 1]
        } else {
            leafs[stateNumber].trans[keys[keyNumber]!] = leafs[st[col]! - 1]
        }
    }
}

var currentStr = ""

let root = leafs[0]
var current = root

let type = Set<String> (["int", "float", "double"])

let keywords = Set<String> (["var", "let", "for", "if", "else", "guard", "while", "in", "return", "=", "{", "}", "+", "-", "*", "/", "%", "int", "float", "double"])


struct LexerComponent {
    var isKeyword: Bool
    var id: Int
    var payload: String
}

var lexerComponents = [LexerComponent]()


func step(ch: Character, line: Int, position: Int) -> Bool {
    if !Basic.shared.isDelimiter(ch) {
        currentStr.append(ch)
    }
    if let next = current.trans[ch] {
        current = next
    } else if let next = current.any {
        current = next
    } else if keywords.contains(currentStr) {
        lexerComponents.append(LexerComponent(isKeyword: true, id: -1, payload: currentStr))
//        print("Keyword: \(currentStr)")
        currentStr = ""
        current = root
        if ch == ":" || ch == ";" || ch == "," {
            lexerComponents.append(LexerComponent(isKeyword: true, id: -2, payload: String(ch)))
        }
        
        return true
    } else if !currentStr.isEmpty {
        print("Error: [\(line), \(position)]\n\"\(currentStr)\"")
        currentStr = ""
        current = root
        return true
    }
    
    if current.finalId != 0 {
        if current.finalId == 1 && keywords.contains(currentStr) {
            lexerComponents.append(LexerComponent(isKeyword: true, id: -1, payload: currentStr))
//            print("Keyword: \(currentStr)")
        } else if current.finalId != 2 && current.finalId != 3 {
            lexerComponents.append(LexerComponent(isKeyword: false, id: current.finalId, payload: currentStr))
//            print("Id: \(current.finalId). Str: \(currentStr)")
        }
        currentStr = ""
        current = root
    }
    if ch == ":" || ch == ";" || ch == "," {
        lexerComponents.append(LexerComponent(isKeyword: true, id: -2, payload: String(ch)))
    }
    return false
}


guard let dir = FileManager.default.urls(for: .desktopDirectory, in: .userDomainMask).first else {
    fatalError()
}

let fileURL = dir.appendingPathComponent("code.txt")

//writing
do {
    var lineNumber = 1
    var position = 1
    var isInTree = false
    
    let inputString = try String(contentsOf: fileURL, encoding: .utf8)
    
    for ch in inputString {
        if ch == "\n" {
            lineNumber += 1
            position = 1
        } else {
            position += 1
        }
        
        if !isInTree && Basic.shared.isDelimiter(ch) {
            continue
        }
        
        isInTree = !step(ch: ch, line: lineNumber, position: position)
    }
} catch {/* error handling here */}

print(lexerComponents)

func isVar(components: [LexerComponent], pos: Int) -> Bool {
    guard components[pos].isKeyword && components[pos].payload == "var" else {
        return false
    }
    
    guard components[pos + 1].isKeyword && type.contains(components[pos + 1].payload) else {
        return false
    }
    
    guard components[pos + 2].isKeyword && components[pos + 2].payload == ":" else {
        return false
    }
    
    return isIdList(components: components, pos: pos + 3)
}

func isIdList(components: [LexerComponent], pos: Int) -> Bool {
    return components[pos].id == 1 && isA(components: components, pos: pos + 1)
}

func isA(components: [LexerComponent], pos: Int) -> Bool {
    guard pos < components.count else {
        return false
    }
    if components[pos].id == -2 && components[pos].payload == ";" {
        return true
    }
    
    guard components[pos].id == -2 && components[pos].payload == "," && components[pos + 1].id == 1 else {
        return false
    }
    
    return isA(components: components, pos: pos + 2)
}

print(isVar(components: lexerComponents, pos: 0))
