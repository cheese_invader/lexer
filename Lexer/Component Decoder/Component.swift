//
//  Component.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct Component: Decodable {
    // Header
    let id: Int
    let description: String
    
    // Helpers
    let stateNumber: Int
    let symbolNumber: Int
    let basics: [BasicComponent]
    let symbols: [SymbolComponent]
    let finals: [Int]
    let transitions: [TransitionConponent]
}
