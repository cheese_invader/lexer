//
//  ComponentType.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

enum ComponentType: String, Decodable {
    case letter
    case digit
    case digit16
    case digit8
    case digit2
    case delimiter
    case any
}
