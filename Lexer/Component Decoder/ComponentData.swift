//
//  ComponentData.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct ComponentData: Decodable {
    let component: Component
}
