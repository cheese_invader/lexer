//
//  Basic.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class Basic {
    private static func readFileInRule(_ path: String) -> String {
        let fileURL = Config.shared.rulesDir.appendingPathComponent(path)
        
        do {
            return try String(String(contentsOf: fileURL, encoding: .utf8).dropLast())
        } catch {
            print("Basic - readFileInRule - " + error.localizedDescription)
            fatalError()
        }
    }
    
    private init() {}
    
    
    private static let digit     = Set<Character>(Basic.readFileInRule("digit.dat"))
    private static let digit16   = Set<Character>(Basic.readFileInRule("digit16.dat"))
    private static let digit8    = Set<Character>(Basic.readFileInRule("digit8.dat"))
    private static let digit2    = Set<Character>(Basic.readFileInRule("digit2.dat"))
    private static let letter    = Set<Character>(Basic.readFileInRule("letter.dat"))
    private static let delimiter = Set<Character>(Basic.readFileInRule("delimiter.dat"))
    
    
    static let shared = Basic()
    
    func isDigit(_ ch: Character) -> Bool {
        return Basic.digit.contains(ch)
    }
    
    func isLetter(_ ch: Character) -> Bool {
        return Basic.letter.contains(ch)
    }
    
    func isDelimiter(_ ch: Character) -> Bool {
        return Basic.delimiter.contains(ch)
    }
    
    func isAny(_ ch: Character) -> Bool {
        return ch == Config.shared.anyChar
    }
    
    func getCharsByType(_ type: ComponentType) -> [Character] {
        switch type {
        case .letter:
            return Array(Basic.letter)
        case .digit:
            return Array(Basic.digit)
        case .digit16:
            return Array(Basic.digit16)
        case .digit8:
            return Array(Basic.digit8)
        case .digit2:
            return Array(Basic.digit2)
        case .delimiter:
            return Array(Basic.delimiter)
        case .any:
            return [Config.shared.anyChar]
        }
    }
}
