//
//  TreeDeterminator.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class TreeDeterminator {
    private let table: [[Set<Int>]]
    private let keys: [Character: Int]
    private let finals: [Int: Int]
    
    private var workspace = [[Set<Int>]]()
    private var states = [Set<Int>]()
    private var stateByMultiple = [Set<Int>: Int]()
    
    private var queue = [Set<Int>]()
    
    init(table: [[Set<Int>]], keys: [Character: Int], finals: [Int: Int]) {
        self.table = table
        self.keys = keys
        self.finals = finals
        determine()
    }
    
    var transitTable: [[Set<Int>]] {
        return workspace
    }
    
    var stateList: [Set<Int>] {
        return states
    }
    
    var indexByState: [Set<Int>: Int] {
        return stateByMultiple
    }
    
    private func determine() {
        if (table.isEmpty) {
            print("TreeDeterminator - determine - Table is empty")
            return
        }
        
        let firstInQueue = expandIfNeed([0])
        queue = []
        queue.append(firstInQueue)
        var workspaceRow = composeWorkspaceRow(states: queue.removeFirst())
        
        while !queue.isEmpty {
            workspaceRow = composeWorkspaceRow(states: queue.removeFirst())
            workspace.append(workspaceRow)
        }
    }
    
    private func composeWorkspaceRow(states: Set<Int>) -> [Set<Int>] {
        var out = [Set<Int>]()
        
        for i in 0..<table[0].count {
            var components = Set<Int>()
            for state in states {
                components.formUnion(table[state][i])
            }
            out.append(expandIfNeed(components))
        }
        return out
    }
    
    private func expandIfNeed(_ state: Set<Int>) -> Set<Int> {
        if state.isEmpty {
            return []
        }
        if let existState = stateByMultiple[state] {
            return states[existState]
        }
        
        var visited = Set<Int>()
        let expand = findExpandState(state, visited: &visited)
        
        states.append(expand)
        queue.append(expand)
        stateByMultiple[state] = states.count - 1
        
        return expand
    }
    
    private func findExpandState(_ states: Set<Int>, visited: inout Set<Int>) -> Set<Int> {
        var newStates = Set<Int>()
        
        for state in states {
            visited.insert(state)
            if table.count <= state {
                print("TreeDeterminator - findExpandState - Index is out of bounds: \(state)")
                continue
            }
            
            for st in table[state][0] where !visited.contains(st) {
                newStates.insert(st)
            }
        }
        
        return newStates.isEmpty ? states : states.union(findExpandState(newStates, visited: &visited))
    }
}
