//
//  Leaf.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class Leaf {
    init() {}
    
    init(id: Int) {
        self.id = id
    }
    
    var id = -1
    var trans = [Character: Leaf]()
    var finalId = 0
}
