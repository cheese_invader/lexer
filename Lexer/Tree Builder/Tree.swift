//
//  Tree.swift
//  Lexer
//
//  Created by Marty on 20/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class Tree {
    private let root = Leaf()
    private var leafId = 0
    
    private let id: Int
    
    private let stateNumber: Int
    
    private var finals: [Int]
    private var transitions: [TransitionConponent]
    
    private var symbols = [Int: Character]()
    private var basics  = [Int: [Character]]()
    
    private var visited = Set<Int>()
    
    
    
    init(_ component: Component) {
        id = component.id
        stateNumber = component.stateNumber
        
        finals  = component.finals
        transitions = component.transitions
        
        symbols = buildSymbols(component)
        basics  = buildBasics(component)
        
        compose()
    }
    
    func getRoot() -> Leaf {
        return root
    }
    
    
    private func compose() {
        var leafs = [root]
        
        for _ in 1..<stateNumber {
            leafs.append(Leaf())
        }
        
        for transit in transitions {
            setLetter(from: leafs[transit.from], to: leafs[transit.to], letter: transit.letter)
        }
        
        for final in finals {
            leafs[final].finalId = id
        }
        leafId = leafs.count
    }
    
    private func setLetter(from: Leaf, to: Leaf, letter: Int) {
        if let chars = basics[letter] {
            for char in chars {
                from.trans[char] = to
            }
        } else if let char = symbols[letter] {
            from.trans[char] = to
        } else {
            print("Tree - setLetter - Undefined symbol id: \(letter)")
        }
    }
    
    private func buildSymbols(_ component: Component) -> [Int: Character] {
        var symbols = [Int: Character]()
        for ch in component.symbols {
            guard let char = ch.value.first else {
                print("Tree - buildSymbols - Wrong symbol. Id: \(ch.id)")
                fatalError()
            }
            symbols[ch.id] = char
        }
        return symbols
    }
    
    private func buildBasics(_ component: Component) -> [Int: [Character]] {
        var basics = [Int: [Character]]()
        for basic in component.basics {
            basics[basic.id] = Basic.shared.getCharsByType(basic.value)
        }
        return basics
    }
}
