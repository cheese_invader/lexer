//
//  FinalLeaf.swift
//  Lexer
//
//  Created by Marty on 22/01/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class FinalLeaf {
    var finalId = 0
    var any: FinalLeaf?
    var trans = [Character: FinalLeaf]()
}
